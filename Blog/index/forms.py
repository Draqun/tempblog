#-*- coding: utf-8 -*-
from django import forms
from django.forms import ModelForm
import models
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError

class NewsForm(ModelForm):
  class Meta:
    model = models.Note
    fields = ['topic', 'note']
    labels = {
              'topic':_(u'Temat'),
              'note':_(u'Wiadomość'),
              }
    
class NewsFormEdit(ModelForm):
  class Meta:
    model = models.Note
    fields = ['topic', 'note', 'date_change']
    labels = {
              'topic':_(u'Temat'),
              'note':_(u'Wiadomość'),
              }
    widgets = {
               'date_change':forms.HiddenInput(),
               }
    
class NewsTagForm(forms.Form):
  tags = forms.CharField(required=False, max_length=255, label=_(u'Tagi'))
  
  def clean_tags(self):
    tags = self.cleaned_data.get('tags').split(';')
    if len(tags)>5:
      raise ValidationError(_(u'Podano za dużo tagów!'))
    return self.cleaned_data.get('tags')


class LoginForm(forms.Form):
  username = forms.CharField(max_length=30, label=_(u'Login'))
  password = forms.CharField(
                          max_length = 32,
                          label=_(u'Hasło'),
                          widget = forms.PasswordInput(),
                          )
