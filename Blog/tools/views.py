#-*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic.base import View
from django.core.paginator import Paginator
from django.http import Http404

class BaseView(View):
  template_name=None
  model=None
  view_data={}
  where={}
  order_by=[]
  back_url=None
  item_for_page=None
  page_view=None
  default_page_number=None
  footer_value=None
    
  def get(self, request, *args, **kwargs):
    page = None
    if 'page' in kwargs and kwargs['page'] != u'':
      page = int(kwargs['page'])
    data=[]
    if self.model and self.model is not None:
      try:
        order_by=[]
        if 'sort' in kwargs and kwargs['sort']!='':
          order_by.append(kwargs['sort'])
        if 'where' in kwargs and kwargs['where']!='':
          self.where.update(kwargs['where'])
        if len(self.order_by)>0:
          order_by.extend(self.order_by)
        if len(self.where)>0 and len(order_by)>0:
          data = self.model.objects.filter(**self.where).order_by(*order_by)
        elif len(self.where)>0:
          data = self.model.objects.filter(**self.where)
        elif len(order_by)>0:
          data = self.model.objects.all().order_by(*order_by)
        else:
          data = self.model.objects.all()
      except:
        return render(request, 'info.html', {'back_url':self.back_url, 'errors':[_(u'Błąd w klasie BaseView.'), ]})
    paginator = Paginator(data, self.item_for_page)
    if page and paginator.num_pages >= page:
      data = paginator.page(page)
    elif paginator.num_pages >= self.default_page_number:
      data = paginator.page(self.default_page_number)
    else:
      raise Http404()
    self.view_data.update({'page':page, 'paginator':paginator, 'data':data, 'page_view':self.page_view, 'footer_value':self.footer_value,'request':request})
    